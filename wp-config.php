<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the installation.
 * You don't have to use the web site, you can copy this file to "wp-config.php"
 * and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'pabau' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', '' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The database collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication unique keys and salts.
 *
 * Change these to different unique phrases! You can generate these using
 * the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}.
 *
 * You can change these at any point in time to invalidate all existing cookies.
 * This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'S)je3zDi0OAS`M}3xY1UO~aoE.92(Bz4P:Zk3[kVQ]|0v`V679n.*i%cFeus H+3' );
define( 'SECURE_AUTH_KEY',  ' :tXM@3q-oNP]XDM4nt<HB5/7X:V.4bnc=T*T5bZ z^_1aJQtSZiSh<nyhOKEE[a' );
define( 'LOGGED_IN_KEY',    '6$b*t+_K4vhSCPzra+,|9Jv]@Y27JS>Z: Yb`Yc;!g}{IK8jIF3::,-v#6bxY{3a' );
define( 'NONCE_KEY',        ']hmLo}Miu+wVmP_*RM;Yg>sd&$NK(. [?iHzMoBpWgpy>0ocMlwgNA$UpZ%1YM*n' );
define( 'AUTH_SALT',        '09&grcxt5y^oxkSh&Jqiet[%^oCDv{3Z#hauL0fNL{7o{s4wS%x]lNGfmg2]NdYW' );
define( 'SECURE_AUTH_SALT', 'm^VtCOQ);`AK|x7}jKh 4|c+uN[# :b,M !-lYtH.>[?m4JjHe7Q+Hg1s2+k|1Ne' );
define( 'LOGGED_IN_SALT',   '=&Ll`cDmm36=tzIb58_Q8]nn-J$yS@_.||h_v#uD#CaM2;6:e:dM;%D3t|}loOS@' );
define( 'NONCE_SALT',       'u}af@O{[C;+8yc@* |)sH#95B_X|4hPhqXl&5%$iLBCc{# &N{>[Fy*c=db)tYHB' );

/**#@-*/

/**
 * WordPress database table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* Add any custom values between this line and the "stop editing" line. */



/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
